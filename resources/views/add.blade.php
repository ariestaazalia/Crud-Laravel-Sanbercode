@extends('main')

@section('card-title')
    <h3>Add Questions</h3>
@endsection

@section('content')
    <form class="contact-form" method="post" action="/pertanyaan">
        @csrf
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="title">Judul:</label>
                    <input type="text" class="form-control" name="title" placeholder="Masukkan Judul">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="title">Isi:</label>
                    <textarea class="form-control textarea" rows="3" name="content"></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary">Send the Question</button>
            </div>
        </div>
    </form>
@endsection