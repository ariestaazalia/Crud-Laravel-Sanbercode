@extends('main')

@section('card-title')
    <h3>Data of <b> {{$pertanyaan->title}} </b></h3>
@endsection

@section('content')
<form>
    @csrf
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <input type="text" class="form-control" name="title" value="{{$pertanyaan->title}}" disabled>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <textarea class="form-control textarea" rows="3" name="content" disabled>{{$pertanyaan->content}}</textarea>
            </div>
        </div>
    </div>
</form>
@endsection