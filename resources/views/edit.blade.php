@extends('main')

@section('card-title')
    <h3>Edit Data For <b> {{$pertanyaan->title}} </b></h3>
@endsection

@section('content')
<form class="contact-form" method="POST" action="/pertanyaan/{{$pertanyaan->id_pertanyaan}}">
    @csrf
    @method('PUT')
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="title">Judul:</label>
                <input type="text" class="form-control" name="title" value="{{$pertanyaan->title}}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="title">Isi:</label>
                <textarea class="form-control textarea" rows="3" name="content">{{$pertanyaan->content}}</textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary">Update the Question</button>
        </div>
    </div>
</form>
@endsection