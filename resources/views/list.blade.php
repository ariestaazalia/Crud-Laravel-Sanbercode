@extends('main')

@section('card-title')
    <a class="btn btn-info" href="/pertanyaan/create">Add Data</a>
@endsection

@section('content')
<div class="card">
    <div class="card-body">
    <table id="question" class="table table-hover">
        <caption> List of Questions </caption>
        <thead>
            <tr>
                <th width="5%">No.</th>
                <th width="20%">Judul</th>
                <th width="40%">Pertanyaan</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($pertanyaan as $item=>$val)
                <tr>
                    <td> {{$item+1}} </td>
                    <td> {{$val->title}} </td>
                    <td> {{$val->content}} </td>
                    <td> <a href="/pertanyaan/{{$val->id_pertanyaan}}" class="btn btn-outline-info"><i class="far fa-eye"></i> Show</a> </td>
                    <td> <a href="/pertanyaan/{{$val->id_pertanyaan}}/edit" class="btn btn-outline-success"><i class="far fa-edit"></i> Edit</a> </td>
                    <td> <form action="/pertanyaan/{{$val->id_pertanyaan}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-outline-danger"><i class="far fa-trash-alt"></i> Delete</button>
                        </form> 
                    </td>
                </tr>
            @empty
                
            @endforelse
            
        </tbody>        
    </table>
    </div>
    <!-- /.card-body -->
</div>
@endsection