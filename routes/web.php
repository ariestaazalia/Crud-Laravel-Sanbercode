<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/pertanyaan', 'PertanyaanController@index');
Route::post('/pertanyaan', 'PertanyaanController@store');
Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::delete('/pertanyaan/{id_pertanyaan}', 'PertanyaanController@destroy');
Route::get('/pertanyaan/{id_pertanyaan}', 'PertanyaanController@show');
Route::get('/pertanyaan/{id_pertanyaan}/edit', 'PertanyaanController@edit');
Route::put('/pertanyaan/{id_pertanyaan}', 'PertanyaanController@update');
